# LP Timelock
Intended for locking FA2 tokens in the Quipuswap LP until the specified deadline.


## How to use

### Project admin
1. After agreeing to the OTC deal, originate the timelock contract. The initial storage should contain the addresses of the token contract, the DEX contract (from Quipuswap pair info), the deadline (you can use unix timestamp), the boolean state should start as `False`, the owner address is the buyer and the admin is you. Token ID of your contract is obvious.
2. Wait for buyer to send you agreed funds for the tokens.
3. Send the agreed number of tokens to the timelock contract
4. Send the address of the timelock contract to the buyer and wait for them to use it.
5. If the deal is off, use `%cancel` to retrieve the tokens. Remember to specify the number of tokens (as a nat).

### Buyer
1. Wait for admin to give you the address of the timelock contract.
2. Check with an explorer like BCD that there are the agreed number of tokens in the contract, the deadline, token address and token ID are correct and your own address is the owner.
3. Go to Quipuswap, check in the Add liquidity section how much XTZ is needed to go with the agreed number of tokens.
4. Call `%lock_xtz` of the timelock contract, use the number of XTZ from Quipuswap as the XTZ amount for this transaction. Remember to specify the agreed number of tokens in the argument of the transaction.
5. Now the tokens and your XTZ are in the LP pool.
6. After the deadline, you can withdraw the LP tokens by calling `%withdraw_all`. Remember to specify the number of LP tokens. Get the number by looking for the balance of the timelock contract in the DEX contract (with an explorer). Get the DEX contract address from the pair info on Quipuswap.

## Example usage
### Deploy
```
./tezos-client -E https://florencenet.smartpy.io/ originate contract lp_timelock_test_7 transferring 0 from florence_alice running contracts/lp_timelock.tz --init 'Pair "KT1VCczKAoRQJKco7NiSaB93PMkYCbL2z1K7" "KT1EqM64bQFPszMqSKDP7SqzTjMfvaLd4nk1" 1619890860 False "tz1Mjd6DjfxPpo6NPUKV4PhA1242Pe5sFqoE" "tz1MZD3EecfFVHbteFYXZMpFnvFH1g6a2BA1" 0' --burn-cap 4
```

### Adding tokens
You can send them using a normal wallet to the timelock address.

### Locking XTZ (start LP)
```
./tezos-client -E https://florencenet.smartpy.io/ transfer 0.384467 from florence_bob to lp_timelock_test_7 --arg '12321' --entrypoint lock_xtz --burn-cap 1
```

### Withdrawing LP tokens (end LP)
```
./tezos-client -E https://florencenet.smartpy.io/ transfer 0 from florence_bob to lp_timelock_test_4 --arg '58330' --entrypoint withdraw_all --burn-cap 1
```
